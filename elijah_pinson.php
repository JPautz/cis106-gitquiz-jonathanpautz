<header>
    <h1>Elijah (Eli) Pinson</h1>
    <p>
        Hey there, I'm Eli. I'm a freshman at Cuesta College and a full-time student. My major is computer science, but I'm not sure where I want to apply that degree.
        I spend a lot of time reading and playing video games. And, though I only recently started, I play bass guitar. My days are fairly long and uneventful,
        which isn't much to complain about. Bus rides are the most time-consuming part of my week. They're also why I've been reading more often, and they persuaded me 
        to purchase a Kindle. My brother, Calvin, convinced me to learn to code. Now I know a fair amount of python, html, and java. I hope to add php to my knowledge-base.
    </p>
</header>
<body>
    <p>
        The best pizza isn't a single-topping pizza. It's a pizza that is divided equally into the golden three types: cheese, Hawaiin, and pepperoni. 
        Eating only one flavor would tire the tongue out, reducing the enjoyment of the pizza. By having three flavors, the tongue -- and by association,
        the brain -- never lose interest in the pizza. 
    </p>
    <p>
        My favorite place to go for vacation is the mountains. I love playing in the snow, sledding, and snowboarding. The cold hurts my fingers, but it makes
        warming up with some homemade hot cocoa even better. Snow-covered areas tend to be pretty, especially snow-laden evergreen tree forests. 
    </p>
</body>

