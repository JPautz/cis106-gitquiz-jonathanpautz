<header>
    <h1>Haaken Baldwin</h1>
    <p>Hello, my name is Haaken Baldwin. I'm a a second-year student at Cuesta College, majoring in computer science. I was originally hoping to transfer to Cal Poly, but my distaste for calculus is such that I think I might transfer to a school that doesn't require linear analysis. I'm also a big fan of birds.  </p>
</header>

<p>I'm not a huge fan of pizza anymore, but if I had to pick a favourite, it would be the sticky fingers pizza from Fatte's Pizza in San Luis Obispo. I haven't eaten pizza in a year or more, but if memory serves me correctly, that pizza suited me best. I am partial to the olives and mushrooms it uses as toppings. There's not really too much more to say, I'm no pizza critic. </p>
<p>I don't really have a favourite place to go for vacation. My family has a vacation home down in Big Bear, California, but I don't particularly like it there. I love the birds there, but that's about the only thing that I enjoy. I tend to prefer to stay home, since I get bored while traveling.</p>
