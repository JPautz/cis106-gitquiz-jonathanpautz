<head>
    <h1>Mack Brown</h1>
    <p>
        My name is Mack. I work for campus dining at Cal Poly. I enjoy playing 
        video games and electronic music. My love for junk food is why I will 
        probably never have a six pack. I hope to get a job somewhere in the tech
        sector programming or scripting for a project that I'm passionate about.
    </p>
    
    <p>
        My favorite kind of pizza is a white sauce, chicken and mushroom. The 
        sauce at most places is a alfredo sauce but its still the best in my
        opinion. Seasoned crust with the toppings makes an amazing combo that 
        no other pizza can beat.
    </p>
    
    <p>
        My favorite place to go for vacation is one the most people would question,
        it's Los Angeles. The reason its my favorite is because I go there in the 
        summer for a convention called Anime Expo. Its a 4 day long convention with 
        everything from cosplayers to fireworks to some of the best food trucks 
        around. Only gripe about the trucks is that they're damn expensive. The 
        convention had roughly 100,000 attendees for 2015 and I plan to make 2016
        even better.
    </p>
</head>