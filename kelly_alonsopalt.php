<h1>Kelly Alonso-Palt</h1>
<p> 
I am the kind of person who dislikes writing descriptions about myself. I am a Cuesta student, but not really. 
I clean my house, but it is not that clean. I cook, but not every meal is worthy of blogging. I don't blog. I have chickens,
but not that many and I don't put diapers in them and let them roam around the house or anything. I do have many, many worms. They too
live outside. I only have one husband and two children and one cat. I will probably have the same husband and children and cat for the rest 
of my life, though in the cat's case, he will eventually stop purring and gaining weight and breathing. My house is small and crappy and costs almost 
every penny we will ever have. It too will be with me for the rest of my life. Rather than remodel like most people do, I will just wait
until 1980s track house becomes fabulous again and then I will call it vintage. 
</p>

<p>
My favorite place to go for vacation is camping in the forest with most of my aunts and uncles and cousins and siblings. Right now I'm 
trying to get everyone on board to go to Huntington Lake. 
</p>
<p> 
My favorite pizza is hot and fresh and usually has white sauce and chicken and a normal crust. 
</p>