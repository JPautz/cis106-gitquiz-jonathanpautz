<h1>Lexa Hall</h1>
<p>I currently live, and have lived my entire life, in San Luis Obispo. I absolutely love it here and I can't
imagine a place I would have rather grown up. At the moment I am attending Cuesta College, but I hope to transfer
to Cal Poly in the fall (fingers crossed that I'll get accepted). In addition to school, I also have several jobs.
The first of which is working as a intern in the development department at TransUnion Interactive. Second, I do
quality assurance testing at iFixit. And finally, to keep things a bit interesting, I work as a floral designer
on the weekends. Between work and school I keep myself fairly busy, but in the small amount of free time that I do
have I like to get outdoors and do something active, be it hiking, biking, rock climbing, or surfing - I just like
to get some time away from my desk and computer.</p>
<p>It's difficult to pick a favorite type of pizza; basically anything served on bread and covered in cheese is going
to be delicious. However, I think that if I had to narrow it down, I'd say my favorite pizza has white sauce with
chicken, bacon, mushrooms, and onions.</p>
<p>When I get a chance to go on vacation I usually try to visit somewhere that I haven't been before, so I do not
really have a 'favorite' place to go. However, out of the places that I have visited, my favorite so far was an
island in the Mediterranean Sea, near Spain, called Majorca.</p>

