<?php
    <h1>Rachel Peterson</h1>
    <p>I am currently taking some computer science classes at Cuesta.  I also work full-time at iFixit 
        doing a mixture of project and product management, and overseeing the manual QA team, among
        other things.
    </p>
    <p>My favorite place to go on vacation...hmm, that's hard.  It's been a really long time since I've 
       actually gone anywhere on vacation.  The best vacation  ever had though was on Hawai'i; it was 
       very lovely and I never wanted to come home.
    </p>
    <p>My favorite pizza is jalapeno, pepperoni, and onion, or just plain cheese, like a little kid.
       Tyler always has so many class examples and test questions that deal with pizza. Now I want pizza.
    </p>  
    


?>